from src.utils.load_data import *


def get_variable(name: str, value: str) -> dict:
    """根据变量名`name`和取值`value`来获得变量完整的结构体"""
    assert name in variables.keys(), f"`{name}`不在变量定义表内"
    assert value in variables[name]["Value"], f"`{value}`不在变量`{name}`的取值范围中."
    temp: dict = variables.get(name)
    variable = dict()
    variable["Name"] = name
    variable["Type"] = temp["Type"]
    variable["Value"] = value
    variable["Remark"] = temp["Remark"]
    return variable


def get_io_variable(jmsgs: list) -> list:
    """根据给定的消息序列（有序），生成定义好的json格式"""
    if len(jmsgs) == 0:
        return list()
    result = list()
    counter = 1
    for jmsg in jmsgs:
        dct = dict()
        temp = []
        label, sub_label = jmsg[1:].split(".")  # 去除开头的J
        temp.append(get_variable("MessageLabel", label))
        temp.append(get_variable("MessageSubLabel", sub_label))
        dct[str(counter)] = temp
        result.append(dct)
        counter += 1
    return result


def get_comparison_obj2obj(param1, param2, operation_type, logical_type, result):
    variable = dict()
    variable["Name"] = "ComparsionObj2Obj"
    variable["Type"] = "Template"
    variable["Value"] = list()
    variable["Value"].append(get_variable(param1, "Default"))
    variable["Value"].append(get_variable(param2, "Default"))
    variable["Value"].append(get_variable("OperationType", operation_type))
    variable["Value"].append(get_variable("LogicalType", logical_type))
    assert result.isdigit(), "5th parameter is illegal! A digit is need."
    variable["Value"].append({"Name": "Result", "Type": "int", "Value": result, "Remark": "表示比较的结果"})

    return variable


def get_comparison_obj2number(param, logical_type, number):
    variable = dict()
    variable["Name"] = "ComparsionObj2Number"
    variable["Type"] = "Template"
    variable["Value"] = list()
    variable["Value"].append(get_variable(param, "Default"))
    variable["Value"].append(get_variable("LogicalType", logical_type))
    variable["Value"].append({"Name": "Number", "Type": "int", "Value": number, "Remark": "表示比较的结果"})

    return variable


def generate_rule_template(condition: list = list(),
                           do: list = list(),
                           i: list = list(),
                           o: list = list(),
                           description: str = str(),
                           rule_id: str = str()) -> dict:
    """生成规则json模板"""
    result = dict()
    result["Condition"] = condition
    result["Do"] = do
    result["Input"] = i
    result["Output"] = o
    result["Description"] = description
    result["ID"] = rule_id
    return result


def split2parts(rule: str):
    """ 通过关键字将规则分成两部分，要注意不同的关键字切分后对应的condition-do不同
    :param rule: 规则文本
    :return:
    """
    split_words_t_h = ["if", "when"]
    doc = nlp(rule)
    result = dict()
    result['sent'] = str(list(doc.sents)[0])  # 只处理第一个句子
    result['word'] = [token.text for token in doc]
    result['condition'] = str()
    result['do'] = str()
    result['split_word'] = str()
    for split_word in split_words_t_h:
        if split_word in result['word']:
            temp = result['sent'].split(split_word)
            result['do'], result['condition'] = temp[0], temp[1]
            result['split_word'] = split_word
            break
    return result['condition'], result['do'], result['split_word']


def remove_space(sent: str, to_lower: bool) -> str:
    """预处理, 清除多余的空格"""
    doc = nlp(sent)
    ls = []
    for token in doc:
        ls.append(token.text)
    ls = [x.strip() for x in ls if x.strip() != '']
    sent = " ".join(ls)
    if to_lower:
        sent = sent.lower()
    return sent


def get_first_sentence(rule: str, to_lower: bool = False) -> str:
    """获取一段话的第一个句子"""
    doc = nlp(rule)
    first_sent = str(list(doc.sents)[0])
    return remove_space(first_sent, to_lower)


def get_receive_json(rule: str, uncase: bool = False) -> list:
    """ 收到消息的json"""
    if uncase:
        rule = rule.lower()
    keyword = "SYNONYM@SUBJECT#Receive@INDEX#3"

    result = list()
    flag: bool = False

    for synomym in synomys2state_variable[keyword]['Synomyms']:
        if uncase:
            synomym = synomym.lower()
        if rule.find(synomym) != -1:
            for k, v in synomys2state_variable[keyword]['VariableValue'].items():
                result.append(get_variable(k, v))
            flag = True
            break
        if flag:
            break

    return result


def get_local_json(rule: str, uncase: bool = False) -> list:
    if uncase:
        rule = rule.lower()
    keyword = "SYNONYM@SUBJECT#Local@INDEX#4"

    result = list()
    flag: bool = False

    for synomym in synomys2state_variable[keyword]['Synomyms']:
        if uncase:
            synomym = synomym.lower()
        if rule.find(synomym) != -1:
            for k, v in synomys2state_variable[keyword]['VariableValue'].items():
                result.append(get_variable(k, v))
            flag = True
            break
        if flag:
            break

    return result
