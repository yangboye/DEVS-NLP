import json

import spacy
nlp = spacy.load('en')

def read_json(filepath: str):
    """ 读取json"""
    with open(filepath, encoding='utf-8') as fp:
        return json.load(fp)


def write_json(data: dict, filepath: str, ensure_ascii: bool = False):
    """ 写入json"""
    with open(filepath, 'w', encoding='utf-8') as fp:
        json.dump(data, fp, ensure_ascii=ensure_ascii, indent=2)


def print_json(data: dict):
    """ 打印json"""
    print(json.dumps(data, indent=2, separators=(',', ':'), ensure_ascii=False))


# ---------------------------
# 加载数据
# ---------------------------
# 同义词 - 状态变量
synomys2state_variable: dict = read_json("./data/synomys2state_variable_table.json")
# 状态变量 - J消息
state_variables2jmsgs: dict = read_json("./data/state_variables2jmsgs_table.json")
# 变量定义
variables: dict = read_json("./data/variables.json")
# 规则
rules: dict = read_json("./data/rules.json")

if __name__ == '__main__':
    data = read_json('./data/ground_truth_json/4.4.4.3.1.c.json')
    condition = data[0]['Condition']
    condition_0 = condition[0]
