from src.utils.process import *
import re


# def get_4_4_4_3_1_c_json(rule_text: str):
#     ori_text : str = rules["AutoCorrelation"]["4.4.4.3.1.c"] # 6016b 原文
#     result = dict()
#     result["SplitWord"] = "upon"
#     result["Sent"] = get_first_sentence(rule_text)
#
#     assert result['SplitWord'] in rule_text, \
#         f"Split word must in rule text. Check your rule description whether likes original 4.4.4.3.1.c: `{ori_text}`"
#
#     parts = result["Sent"].split(result["SplitWord"])
#     result["Condition"], result["Do"] = parts[1], parts[0]
#     result["Input"] = []
#     result["Output"] = []
#     result["Proceed"] = dict()
#     cond_recv =  get_receive_json(result["Condition"])
#     cond_local = get_local_json(result["Condition"])
#     cond_logic = []
#     if result["Condition"].find('real - time') != -1 or result["Condition"].find('real time') != -1:
#         cond_logic = get_comparison_obj2number("RemoteTQ", "!=", "0")
#
#     return result

# ----------------------------
# 自动航迹相关部分
# ----------------------------
def get_4_4_4_3_1_c_json(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/AutoCorrelation/4.4.4.3.1.c.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


def get_4_4_4_3_1_h_json(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/AutoCorrelation/4.4.4.3.1.h.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


def get_4_4_4_3_1_j_json(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/AutoCorrelation/4.4.4.3.1.j.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


def get_4_7_3_b_json(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/AutoCorrelation/4.7.3.b.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


def get_4_7_3_1_c_json(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/AutoCorrelation/4.7.3.1.c.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


# ----------------------------
# R2测试部分
# ----------------------------
def get_4_4_4_14_a(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/R2Test/4.4.4.14.a.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


def get_4_4_4_14_b(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/R2Test/4.4.4.14.b.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id
    return ori_data


def get_4_4_4_14_c(rule_text: str, rule_id: str = None):
    ori_data: dict = read_json("./data/ground_truth_json/R2Test/4.4.4.14.c.json")
    for data in ori_data:
        data["Description"] = rule_text
        if rule_id:
            data["ID"] = rule_id

    # 更新 TQ_L - TQ_R >= 2部分的数值模板
    split_word = "if"
    assert split_word in rule_text, \
        f"Check your description's struct whether likes the original rule text: `{ori_data}`. \n Need to a template likes: [Do]xxx if [Condition]yyy"
    parts = rule_text.split(split_word)
    condition = parts[1]
    match_obj = re.match(r"(.*) exceeds (.*) by (.*) or more", condition, re.M|re.I)
    if match_obj:
        sent_1 = match_obj.group(1).lower()
        sent_2 = match_obj.group(2).lower()
        sent_3 = match_obj.group(3).lower()
        if sent_1.find("local") != -1 and sent_1.find("tq") != -1:
            param1 = "LocalTQ"
            param2 = "RemoteTQ"
        else:
            param1 = "RemoteTQ"
            param2 = "LocalTQ"
        comparision = get_comparison_obj2obj(param1, param2, "Subtraction", ">=", sent_3)
        ori_data[0]["Condition"][0]['1'][2] = comparision
    return ori_data


if __name__ == '__main__':
    # print_json(get_4_4_4_3_1_c_json(""))
    # print_json(get_4_4_4_3_1_h_json(""))
    # print_json(get_4_4_4_3_1_j_json(""))
    # print_json(get_4_7_3_1_c_json(""))
    # print_json(get_4_7_3_b_json(""))
    print_json(get_4_4_4_14_c("A JU assumes R2 on a common track if its received TQ at the time of transmission exceeds the local TQ by 3 or more."))
